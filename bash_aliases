# for when I have emacs installed (almost never)
alias emacs="emacs -nw"

# make editing common files a brise
alias vimrc="vim ~/.vimrc"
# moved for bashrc for compatibility reasons related with zsh
#alias bashrc="vim ~/.bashrc ; source ~/.bashrc"
#alias aliases="vim ~/.bash_aliases ; source ~/.bashrc"
alias profile="vim ~/.profile"
alias i3config="vim ~/.i3/config"
alias resources="vim ~/.Xresources"

alias cls='clear'

# making the sourcing more lightweight
alias sb="source ~/.bashrc"

# make it shorter make it easy
alias sp="echo "${PATH//:/$'\n'}""

# i'm just to lazy
alias nf="neofetch"

# mheck adopted town weather
alias wttr-wob="curl wttr.in/wolfsburg"

# check my hometown weather
alias wttr-lis="curl wttr.in/lisbon"

# simple and fast
alias ..="cd .."

# search for desired file/folder in the current directory
alias fh="find . -name "

# checking out disk usage in human-readable style
alias df="df -Tha --total"

# how about reconstructing the “du” tool output
alias du="du -ach | sort -h"

# let’s make the “free” output friendlier
alias free="free -mt"

# making a directory/folder a little easier, and be notified
alias mkdir="mkdir -pv"

# get rid of command not found
alias cd..='cd ..'

# colorize the grep command output for ease of use
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# need to ^install  colordiff package :)
alias diff='colordiff'

# safety net for removing, moving, copying and linking
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

# convert all webm to mp3 inside current directory
#alias webm2mp3="find . -type f -iname "*.webm" -exec bash -c 'FILE="$1"; ffmpeg -i "${FILE}" -vn -ab 160k -ar 44100 -y "${FILE%.webm}.mp3";' _ '{}' \;"

# change the AppImage file to executable for it to run smoothly
alias setapp="chmod a+x"

# resume by default - This one saved by butt so many times
alias wget='wget -c'

# dealing with the Ipv6 reanabling from the NetworkManager
alias restart-nm="sudo systemctl restart NetworkManager.service"
alias check-ipv6="sysctl net.ipv6.conf.all.disable_ipv6 net.ipv6.conf.lo.disable_ipv6 net.ipv6.conf.enp1s0f1.disable_ipv6 net.ipv6.conf.wlp2s0.disable_ipv6"
