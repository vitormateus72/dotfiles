#!/bin/bash
############################
# .make.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
# Specific files on specific folders must be added individualy (e.g. i3/config
# or config/alacritty/alacritty.yml)
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
i3=~/dotfiles/i3
ala=~/dotfiles/config/alacritty
olddir=~/.dotfiles_old             # old dotfiles backup directory
oldi3=~/.dotfiles_old/i3
oldala=~/.dotfiles_old/config/alacritty
files="bash_aliases bashrc gitconfig profile tmux.conf vimrc xinitrc Xresources zshrc"    # list of files to symlink in homedir

##########

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
mkdir -p $oldi3
mkdir -p $oldala
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file ~/.dotfiles_old/
    mv ~/.i3/config ~/.dotfiles_old/i3/
    mv ~/.config/alacritty/alacritty.yml ~/.dotfiles_old/config/alacritty/
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
    ln -s $i3/config ~/.i3/config
    ln -s $ala/alacritty.yml ~/.config/alacritty/alacritty.yml
echo "All done"
done
