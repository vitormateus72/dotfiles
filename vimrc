"
" ~/.vimrc
"

" Don't try to be vi compatible
set nocompatible

" Helps force plugins to load correctly when it is turned back on below
filetype off

" Try to eliminate the garbage on buffer when opening file
set t_u7=

" TODO: Load plugins here (vim-plug, pathogen or vundle)

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Make sure to use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" On-demand loading; fetches NERDTree / NERDTree Tabs when toggled
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'jistr/vim-nerdtree-tabs', { 'on': 'NERDTreeToggle' }

" My personally added plugins

" airline lightweight status line
Plug 'vim-airline/vim-airline'

" programming specific related plugins
Plug 'tmhedberg/SimpylFold'
Plug 'vim-scripts/indentpython.vim'
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8'
Plug 'davidhalter/jedi-vim'
Plug 'deoplete-plugins/deoplete-jedi'

" section for specefic FZF integration on VIM
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" neoplete-nvim specific section
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

let g:deoplete#enable_at_startup = 1

" colorschemes
Plug 'drewtempelmeyer/palenight.vim'

" easelly comment / uncomment on NORMAL mode
Plug 'tpope/vim-commentary'

" make git usefull within vim
Plug 'tpope/vim-fugitive'

" rainbow brackets
Plug 'frazrepo/vim-rainbow'

" Initialize plugin system
call plug#end()

" enable folding
set foldmethod=indent
set foldlevel=99

" enable folding with the spacebar
nnoremap <space> za

" better command-line completion
set wildmenu

" setting up indendation
au BufNewFile, BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

au BufNewFile, BufRead *.js, *.html, *.css
    \ set tabstop=2 |
    \ set softtabstop=2 |
    \ set shiftwidth=2

highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" turn on syntax highlighting
syntax enable

" set python highlight as default
let python_highlight_all=1

" recommended settings for syntastic plugin
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" run the Flake8 check every time you write a Python file
autocmd BufWritePost *.py call flake8#Flake8()

" for plugins to load correctly
filetype plugin indent on

" TODO: Pick a leader key

" I picked the sugested comma
let mapleader = ","

" :W sudo saves the file when the file is open in readonly mode
command W w !sudo tee % > /dev/null

" security
set modelines=0

" show line numbers in special hybrid auto toggle mode
set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" show file stats
set ruler

" blink cursor on error instead of beeping
set visualbell

" encoding
set encoding=utf-8

" whitespace
set wrap
set textwidth=79
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround

" cursor motion
set scrolloff=3
set backspace=indent,eol,start " more powerful backspacing
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim

" move up/down editor lines
nnoremap j gj
nnoremap k gk

" allow hidden buffers
set hidden

" rendering
set ttyfast

" status bar
set laststatus=2

" last line mode
set showmode
set showcmd

" searching settings
nnoremap / /\v
vnoremap / /\v
set hlsearch " we can hide the search highlight until next search with :noh (Vitor Mateus)
set incsearch
set ignorecase
set smartcase
set showmatch
" not sure what it does, keeping it off for now. (Vitor Mateus)
"map <leader><space> :let @/=''<cr> " clear search

" highlight current line
set cursorline

" more natural split opening
set splitbelow
set splitright

" map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$

" map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>

" Ctrl-j/k deletes blank line below/above, and Ctrl-h/l inserts. Added by Vitor Mateus
nnoremap <silent><C-j> m`:silent +g/\m^\s*$/d<CR>'':noh<CR>
nnoremap <silent><C-k> m`:silent -g/\m^\s*$/d<CR>'':noh<CR>
nnoremap <silent><C-h> :set paste<CR>m'o<Esc>'':set nopaste<CR>
nnoremap <silent><C-l> :set paste<CR>m'O<Esc>'':set nopaste<CR>

" toggle NERDTree mapping for F6 key
" added checking function for security and stability
let g:NetrwIsOpen=0

function! ToggleNetrw()
    if g:NetrwIsOpen
        let i = bufnr("$")
        while (i >= 1)
            if (getbufvar(i, "&filetype") == "netrw")
                silent exe "bwipeout " . i
            endif
            let i-=1
        endwhile
        let g:NetrwIsOpen=0
    else
        let g:NetrwIsOpen=1
        silent Lexplore
    endif
endfunction

noremap <silent> <F6> :call ToggleNetrw()<CR>

" nerd tree settings
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" use F2 to have clean indent on paste from clipboard
set pastetoggle=<F2>
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>

" alternative way to toggle paste mode on/off
set pastetoggle=<leader>p

" remove all trailing whitespace by pressing F5
"nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR> " this was
" a dirty way and mess up with the search history.

" so let us give ourselfes a little work and turn this into a function
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

:call TrimWhitespace()

" since this is somewhat annoying to type :call all the time, we can define a command
command! TrimWhitespace call TrimWhitespace()

" and we can of course bind it to a key, and we will
:noremap <F5> :call TrimWhitespace()<CR>

" some people like to automatically do this before they write a file to disk, not my case!
" I don't like it, as some formats require trailing whitespace (such as Markdown),
" and on some other occasions you even want trailing whitespace in your code
" (such as formatting an email, and using the --<Space> marker to indicate
" the start of a signature).
"autocmd BufWritePre * :call TrimWhitespace()

" enable the use of powerline fonts to airline
let g:airline_powerline_fonts=1

" setup rainbow brackets
let g:rainbow_active = 1

let g:rainbow_load_separately = [
    \ [ '*' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.tex' , [['(', ')'], ['\[', '\]']] ],
    \ [ '*.cpp' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.{html,htm}' , [['(', ')'], ['\[', '\]'], ['{', '}'], ['<\a[^>]*>', '</[^>]*>']] ],
    \ ]

let g:rainbow_guifgs = ['RoyalBlue3', 'DarkOrange3', 'DarkOrchid3', 'FireBrick']
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']

" formatting
map <leader>q gqip

" visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
" uncomment this to enable by default:
" set list " to enable by default
" or use your leader key + l to toggle on/off
map <leader>l :set list!<CR> " Toggle tabs and EOL

" color support (terminal)
set t_Co=256

" set background mode
set background=dark

" set vim colorscheme
colorscheme palenight

" set airline colorscheme
let g:airline_theme = "palenight"
