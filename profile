# setting my default editor to VIM (Vitor Mateus)
export EDITOR=/usr/bin/vim
# ported from .BASHRC to here (Vitor Mateus)
export PAGER=/usr/bin/most
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# fix "xdg-open fork-bomb" export your preferred browser from here (Vitor
# Mateus)
export BROWSER=/usr/bin/firefox
# fixing QT5 applications (like VLC) UI scaling (if icons, fonts are too big)	(Vitor Mateus)
# have commented out the first line as it was a duplicate in this fix (Vitor Mateus)
export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
